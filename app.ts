import * as express from "express";
import * as DEV from "./src/config/dev.json";
import * as ROUTES from "./src/config/routes.json";
import logger from "./src/middleware/logger";
import { authRouter } from "./src/routes/auth";
import { peopleRouter } from "./src/routes/people";

// Config
const app = express();
app.use(logger);
app.use(ROUTES.AUTH, authRouter);
app.use(ROUTES.PEOPLE, peopleRouter);
// Serve static content
//app.use(express.static('./public'));

/* 
    Methods of  express() :

    app.get --> get data list
                            or unique --> path param
                            or sorted list -->  query string
    app.post --> insert data (json)
    app.put --> update specific data (json + path param)
    app.delete --> remove specific data (path param)

    app.all --> handle all methods for endpoint e.g. fallbacks
    app.use --> setting middlewares
    app.listen --> define port to listen to + optional callbacks 
*/

// Fallback
app.all("*", (req, res) => {
    res.status(404).send(res.statusCode + " - Not found");
});

// Listen
const port = process.env.PORT || DEV.PORT;
app.listen(port, () => {
    console.log(`Application running on port ${port}`);
});
