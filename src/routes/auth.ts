import * as express from "express";
import { IPerson } from "../types/iperson";

export const authRouter = express.Router();
authRouter.use(express.json()); // handling JSON

// Login
authRouter.post("/login", (req, res) => {
    const person: IPerson = req.body;
    if (person.name) {
        res.status(200).send("<h1>Home</h1>");
        return;
    }
    res.status(400).json({
        success: false,
        message: "please provide credentials",
    });
});
