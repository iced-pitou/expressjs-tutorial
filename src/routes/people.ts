import * as express from "express";
import {
    createPerson,
    deletePerson,
    getPeople,
    updatePerson,
} from "../controllers/people";

export const peopleRouter = express.Router();
peopleRouter.use(express.json()); // handling JSON

peopleRouter
    .route("/") // Common route
    .get(getPeople) // GET
    .post(createPerson); // POST
peopleRouter
    .route("/:id") // Common route
    .put(updatePerson) // PUT
    .delete(deletePerson); // DELETE
