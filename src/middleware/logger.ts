export default function (req, res, next) {
    const method: string = req.method;
    const url: string = req.url;
    const time: number = new Date().getFullYear();
    console.log(method, url, time);
    next();
}
