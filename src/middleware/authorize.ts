export default function (req, res, next) {
    const user: string = req.query.user;

    if (user === "pitou") {
        console.log("Authorized");
        next();
    } else {
        console.log("Unauthorized");
        res.status(401).send("Unauthorized");
    }
}
