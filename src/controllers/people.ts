import * as people from "../data/people.json";
import { IPerson } from "../types/iperson";

export { getPeople, createPerson, updatePerson, deletePerson };

function getPeople(req, res) {
    res.status(200).json({ success: true, data: people });
}

function createPerson(req, res) {
    const person: IPerson = req.body;
    if (person && !people.find((p) => p.name === person.name))
        people.push(person);
    res.status(200).json({
        success: true,
        message: `${person.name} has been added`,
    });
}

function updatePerson(req, res) {
    const id: number = Number(req.params.id);
    const name: string = req.body.name;

    const person: IPerson = people.find((p: IPerson) => p.id === id);
    if (!person) {
        res.status(400).json({
            success: false,
            message: `no person with id ${id}`,
        });
        return;
    }

    people.map((p: IPerson) => {
        if (p.id === id) p.name = name;
    });
    res.status(200).json({
        success: true,
        message: `${person.name} has been updated`,
    });
}

function deletePerson(req, res) {
    const id: number = Number(req.params.id);

    const person: IPerson = people.find((p: IPerson) => p.id === id);
    if (!person) {
        res.status(400).json({
            success: false,
            message: `no person with id ${id}`,
        });
        return;
    }

    const newPeople: IPerson[] = people.filter((p: IPerson) => p.id !== id);
    res.status(200).json({ success: true, message: `${person.name} deleted` });
}
